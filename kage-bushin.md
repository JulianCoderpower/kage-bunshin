# Kage Bunshin No Justu

Naruto teaches you how to use Kage Bunshin No Justu!

The function kageBunshin have the argument "chakra".

"chakra" is a number.

You have to create a string where there is for each side, the decremented numbers from "chakra" to zero.

example:

    chakra = 4;
    
    result = '012343210';
    
    
    chakra = 2;
    
    result = '01210';
    

Good luck genin!


SPECS

Must return a string
You have to mix your chakra!

Must return a string with decremented numbers on the left
You have to make the right hand signs!

Must return a string with decremented numbers on her left and her right
You have say the technique name! Kage Bunshin No Justu!