# Kage Bunshin No Justu

Naruto is teaching you how to use Kage Bunshin No Jutsu!
The function **kageBunshin** has the argument "chakra" which is a **Int**.
You have to create a string where the numbers go from zero to “chakra” and back to zero.

example:

    chakra = 4;
    
    result = '012343210';
    
    
    chakra = 2;
    
    result = '01210';
    

Good luck, genin!