module.exports = function kageBunshin(chakra){
    var clonesRight = '';
    var clonesLeft = '';
    
    for(var i = 0 ; i < chakra ; i++){
        clonesLeft += i;
    }

    for(var j = chakra - 1 ; j >= 0 ; j--){
        clonesRight += j;
    }

    return clonesLeft.concat(String(chakra), clonesRight);
};